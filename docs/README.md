# SQUARE - Handbuch

## Was ist SQUARE

Bei Square handelt es sich um ein Regelwerk für Tischrollenspiele, auch
Pen-&-Paper-Rollenspiele genannt. 

Square ist ein asymmetrisches Rollenspiel-Regelwerk. Das heißt, dass nicht für alle
Teilnehmer die gleichen Regeln gelten. Wie so oft bei traditionellen Rollenspielen, wird dabei zwischen der Funktion des Spielers und dem des Spielleiters unterschieden. Die beiden Funktionen haben unterschiedliche Aufgaben.

### Die Aufgabe aller Teilnehmer

Die Aufgabe aller Teilnehmer in einem Rollenspiel ist es, gemeinsam eine
fiktive Welt zum Leben zu erwecken, die zu erkunden und kennenzulernen
und darin eine spannende Handlung zu erleben. Dazu gehört das
Ausarbeiten von Charakteren, die im Spiel agieren und die Interaktion
mit anderen Charakteren und NSCs gestalten.

Jeder Teilnehmer hat die Verantwortung, aktiv zum Spiel beizutragen und die Handlung voranzutreiben. Dies beinhaltet das Ausarbeiten von Hintergrundgeschichten für den eigenen Charakter, das Treffen von Entscheidungen, die die Handlung beeinflussen, und das Schaffen von spannenden Interaktionen mit anderen Charakteren und NPCs. 

Ein wichtiger Aspekt ist die Zusammenarbeit aller Teilnehmer. Das Ziel des Spieles 
ist es, gemeinsam eine Handlung gemeinsam eine Geschichte zu erleben, mit all ihren Höhen und Tiefen für die involvierten Charakteren. Eine verlorene Schlacht kann dabei genau so eine Kulisse für gutes Charakterspiel sein, wie ein glorreicher Sieg über einen schier unbezwingbaren Gegner.

Dies erfordert ein hohes Maß an Kommunikation und
Koordination, um die Handlung voranzutreiben und gemeinsam zum Erfolg zu
kommen.

Ein weiterer Aspekt ist das Einhalten der Spielregeln und der
Verhaltenskodex innerhalb der Gruppe. Dies umfasst sowohl die Regeln des
Spiels selbst als auch die zwischenmenschliche Interaktion innerhalb der
Gruppe. Alle Teilnehmer müssen respektvoll miteinander umgehen und
sicherstellen, dass alle Mitglieder der Gruppe in das Spiel einbezogen
werden und ihren Spaß haben können.

Bei Unstimmigkeiten betreffend dem Auslegen der Regeln gilt die
Auslegung des Spielleiters.

!!! note "Wichtig"

    Die Aufgabe aller Teilnehmer ist es, für ein gutes Spiel zu sorgen.
    Was im Spiel geschieht, bleibt im Spiel.

### Die Aufgabe der Spieler


In einem Rollenspiel haben die Spieler die Aufgabe, eine Rolle zu
übernehmen und diese Rolle im Rahmen der Spielhandlung auszufüllen.
Die Spieler haben die Aufgabe, in der Spielwel
Dabei müssen sie unter Berücksichtigung der Persönlichkeit des, die Fähigkeiten und die Ziele ihres
Charakters verkörpern und in der Interaktion mit anderen Spielern und
Nicht-Spieler-Charakteren (NPCs) zum Ausdruck bringen.

Zu den Aufgaben der Charakter-Spieler gehört es, Entscheidungen im Spiel
zu treffen und diese Entscheidungen entsprechend der Persönlichkeit
ihres Charakters zu begründen und umzusetzen. Sie müssen auch darauf
achten, dass sie im Spiel die Fähigkeiten ihres Charakters angemessen
nutzen und weiterentwickeln.

!!! note "Wichtig"

    Die Spieler kümmern sich um ihre Charakter. Sie können nur durch diese
    in die Spielwelt eingreifen. Die Spieler haben den vereinbarten Regeln
    zu folgen und die Moderation des Spielleiters zu achten.


### Die Aufgabe des Spielleiters

Dem Spielleiter obliegt die Moderation des Spieles. Es ist seine
Aufgabe, dafür zu sorgen, dass alle Spieler gleichermaßen die
Möglichkeit haben, mit ihrer Spielfigur - dem Charakter - in den Verlauf
der gemeinsam erzählten Geschichte einzugreifen. Als Moderator ist seine
erste und wichtigste Aufgabe das gute Spiel. Er oder sie moderiert falls
notwendig auch Interaktionen zwischen den Spielern selbst.

Die Beschreibung der fiktiven Welt, in der das Spiel stattfindet ist
ebenfalls die Verantwortung des Spielleiters. Dazu gehört das
Beschreiben von Orten, Städten, Ländern, Kreaturen, Charakteren und
anderen Elementen, die die Spielercharakter während des Spiels begegnen
werden.

Der Spielleiter beschreibt die Handlung des Spieles, in der die Spieler
ihre Charaktere einbringen können. Die Handlung kann aus einer Reihe von
Ereignissen, Aufgaben, Rätseln und Konflikten bestehen, die die Spieler
herausfordern und ihnen die Möglichkeit geben, ihre Charaktere zu
entwickeln und die Spielwelt zu erkunden.

Während des Spiels ist der Spielleiter dafür verantwortlich, die
Handlung voranzutreiben und sicherzustellen, dass die Spieler innerhalb
der Spielregeln agieren. Sie oder er ist verantwortlich für die
Verwaltung der Spielmechanik und das Anwenden der Regeln des Spiels.
Dies umfasst Aspekte wie das Durchführen der Proben.

## Der Charakter

Der Charakter ist die vom Spieler kontrollierte Figur im Rollenspiel.
Jeder Spieler erstellt und entwickelt seinen eigenen Charakter und
verkörpert ihn während des Spiels. Der Charakter hat seine ganz eingene
Kombination von Attributen und Fertigkeiten welchen ihn Spielmechanisch
definieren.

Über die Spielmechanik hinaus hat der Charakter auch narrative und
emotionale Dimensionen. Der Spieler kann seinem Charakter eine
Hintergrundgeschichte geben, ihn mit persönlichen Motivationen, Ängsten
oder Wünschen ausstatten und ihm eine individuelle Persönlichkeit
verleihen. Dadurch wird der Charakter in der Spielwelt verankert und
durch seine Persönlichkeit und seinen Beziehungen in als Teil der
Spielwelt definieren.

### Attribute

Bei Square werden die wichtigsten Charaktereigenschaften durch die neun
Kombinationen von drei Aspekten (mental, emotional, physisch) und drei
Manifestationen (Agilität, Belastbarkeit, Stärke) repräsentiert. Jedes
dieser Attribute deckt dabei einen bestimten Bereich der Möglichkeiten
ab, mit welchen ein Charakter regeltechnisch mit der Spielwelt agieren
kann.

Mentale Agilität

:   Fähigkeit, schnell und effektiv auf neue Situationen zu reagieren,
    durch Kreativität, Initiative, Wahrnehmung und Multitasking.

Emotionale Agilität

:   Fähigkeit, emotionale Reaktionen an eine bestimmte Situation
    anzupassen und Empathie, Intuition und schnelle Reaktionen zu
    nutzen.

Physische Agilität

:   Fähigkeit, die Bewegungen des eigenen Körpers genau wahrzunehmen und
    Koordination, Gleichgewicht und Geschicklichkeit zu nutzen.

Mentale Belastbarkeit

:   Fähigkeit, den Fokus zu halten und sich auf schwierige Aufgaben zu
    konzentrieren, indem man Konzentration, Erinnerungsvermögen,
    Lernfähigkeit und Fokus nutzt.

Emotionale Belastbarkeit

:   Fähigkeit, sich in schwierigen emotionalen Situationen stabil zu
    halten und Willensstärke zu zeigen.

Physische Belastbarkeit

:   Fähigkeit, physischen Stress und Anstrengungen standzuhalten und
    Ausdauer, Kondition und Zähigkeit zu nutzen.

Mentale Stärke

:   Fähigkeit, komplexe Informationen schnell zu verarbeiten und
    analytische und planerische Fähigkeiten, einschließlich Intelligenz,
    Logik und Deduktion, zu nutzen.

Emotionale Stärke

:   Fähigkeit, in sozialen Situationen dominant aufzutreten und Geduld
    und Charisma auszustrahlen.

Physische Stärke

:   Fähigkeit, körperliche Kraft, Geschwindigkeit und Athletik zu
    nutzen, um physische Herausforderungen zu meistern.

!!! admonition "Beispiel"

    Als praktische Darstellung für die Attibute hat sich eine 3x3 grosse
    Tabelle bewährt. Hier ein Beispiel mit

    |                |   **mental**                               |  **emotional**                 |    **physisch**     |
    |----------------|--------------------------------------------|--------------------------------|---------------------|
    | Agilität       | Kreativität, Wahrnehmung, Aufmerksamkeit   | Reaktion, Empathie, Intuition  | Fingergertigkeit, Körperbeherrschung, Akrobatik     |
    | Belastbarkeit  | Konzentration, Erinnerung, Lernen          | Durchhaltewille, Selbstvertrauen, Willensstärke     | Zähigkeit, Kondition, Ausdauer |
    | Stärke         | Intelligenz, Logik, Deduktion   | Durchsetzungsvermögen, Präsenz, Initiative | Körperkraft, Geschwindigkeit, Athletik             |


### Fertigkeiten

Zusätzlich zu den Attributen haben die Charaktere in Square eine
Vielzahl von Fertigkeiten, die ihre spezifischen Fähigkeiten und Talente
darstellen. Jede Fertigkeit hat einen Wert, welcher den Grad oder die
Beherrschung dieser spezifischen Fertigkeit repräsentiert. Angefangen
von einer gedachten Null für Fertigkeiten, die der Charakter überhaupt
nicht beherrscht, können die Fertigkeitswerte bis zu maximal sechs
ansteigen.

Hier eine die geläufige Interpretation dieser Werte.

Fertigkeitswert eins

:   Der Charakter hat grundlegende Kenntnisse oder Fähigkeiten in dieser
    bestimmten Fertigkeit. Er ist Anfänger und noch nicht sehr erfahren
    oder geschickt darin.

Fertigkeitswert zwei

:   Der Charakter hat etwas Erfahrung und Training in dieser Fertigkeit.
    Er ist in der Lage, einfache Aufgaben oder Handlungen in diesem
    Bereich durchzuführen, aber es fehlt ihm noch an Tiefe und
    Perfektion.

Fertigkeitswert drei

:   Der Charakter hat solide Kenntnisse und Fähigkeiten in dieser
    Fertigkeit. Er kann routinemäßige Aufgaben erfolgreich bewältigen
    und ist in der Lage, Herausforderungen zu meistern, die ein
    durchschnittliches Maß an Können erfordern.

Fertigkeitswert vier

:   Der Charakter beherrscht diese Fertigkeit gut und kann auch
    anspruchsvollere Aufgaben erfolgreich bewältigen. Er ist in der
    Lage, kreative Lösungen zu finden und überdurchschnittliche
    Leistungen zu erbringen.

Fertigkeitswert fünf

:   Der Charakter ist ein Experte in dieser Fertigkeit. Er verfügt über
    umfangreiches Wissen und Können und kann selbst schwierige Aufgaben
    oder Situationen mit Leichtigkeit meistern. Seine Fertigkeiten
    übertreffen diejenigen der meisten anderen Charaktere.

Fertigkeitswert sechs

:   Der Charakter ist ein Meister in dieser Fertigkeit. Er ist auf dem
    Höhepunkt seines Könnens und beherrscht diese Fertigkeit nahezu
    perfekt. Seine Leistungen sind außergewöhnlich und er ist in der
    Lage, selbst die anspruchsvollsten Aufgaben mit Bravour zu meistern.

!!! note "Wichtig"

    Die Auswahl der Fertigkeiten ist in der Regel frei, jedoch müssen sie
    spezifisch genug sein, um den Charakter angemessen zu repräsentieren,
    aber nicht so allgemein, dass sie alle Situationen abdecken.

    Beispielsweise ist **Faustkampf** eine spezifische Fertigkeit, die dem
    Charakter erlaubt, im Nahkampf mit bloßen Händen zu kämpfen. Hingegen
    ist **Nahkampf** zu allgemein, da es eine Vielzahl von Waffen und
    Kampftechniken umfasst und somit nicht als Fertigkeit gewählt werden
    kann.

Welche Fertigkeiten einem Spieler zur Ausgestaltung seines Charakters
bereitstehen, kann zum Beispiel durch die Spielwelt eingeschränkt sein.

Letztendlich unterliegt dies dem Ermessen des Spielleiters.

Im folgenden ein paar Beispiele von möglichen Fertigkeiten:

!!! note "Beispiel"

    Hier ein paar Beispiele für Fertigkeiten. Mehr Fertigkeiten können im 
    [Anhang A: Liste mit Fertigkeiten](#liste-mit-fertigkeiten) gefunden werden.

    Taschendiebstahl

    :   Nutzt Fingerfertigkeit, Ablenkung und Tarnung, um unbemerkt
        Gegenstände aus den Taschen anderer zu stehlen oder sie von Juwelen,
        Schmuck und anderen kleinen Dingen zu erleichtern.

    Schlösserknacken

    :   Nutzt Wissen über verschiedene Schlossarten, Fingerfertigkeit und
        Werkzeuggebrauch, um Schlösser zu knacken und Zugang zu
        verschlossenen Räumen und Behältern zu erhalten.

    Erste Hilfe

    :   Befähigt dazu, grundlegende medizinische Hilfe zu leiste; befähigt
        zur Wundversorgung, Diagnose und Verabreichung von Schmerzmitteln
        anwendet.

    Tierkunde

    :   Verleiht Wissen über verschiedene Tiere und deren Verhaltensweisen;
        Erkennen von Tierspuren, Bestimmen von Tieren und Interaktion mit
        Tieren.

    Reiten (Pferde)

    :   Befähigt dazu, ein Pferd zu reiten; Pferdepflege, Satteln und
        Reiten.

### Ein Charakter wird geboren

Bei der Charaktererschaffung werden insgesamt 27 Punkte auf die 9
Attribute verteilt. In jeder Spalte sowie in jeder Zeile muss es ein
dominantes Attribut geben; ein Attribut mit einem höheren Wert, als alle
anderen. Der minimale Wert eines Attributes bei der Charaktererschaffung
ist eins, der maximale Wert ist sechs.

!!! note "Hinweis"

    Einen Charakter kann man schnell erschaffen, indem zuerst auf jedes
    Attribut drei Punkte verteilt und anschliessend wiederholt zwei
    Attribute auswählt und Punkte zwischen den beiden austauscht.


!!! note "Beispiel"

    Tamr ist eine Barbarin aus den südlichen Bergen. **L: Bitte beschreiben**

    | **Tamr**      | mental   | emotional | physisch  |
    |---------------|:--------:|:---------:|:---------:|
    | Agilität      |   2      |   1       |   3       |
    | Belastbarkeit |   2      |   3       |   4       |
    | Stärke        |   3      |   4       |   5       |


Für die Fertigkeiten stehen bei der Charaktererschaffung insgesamt 15
Punkte zur Verfügung. Diese werden auf mindestens 5 Fertigkeiten
verteilt werden.


!!! note "Beispiel"

    Tamr beginnt mit einigen Fertigkeiten

    | Fertigkeit         | Wert  |
    |--------------------|------:|
    | Tierkunde          |   3   |
    | Bogen schiessen    |   3   |
    | Faustkampf         |   3   |
    | Spuren lesen       |   3   |
    | Fallen             |   3   |   


## Interaktion mit der Spielwelt

Jeder Charakter kann über seine Fertigkeiten mit der Spieltwelt interagieren.
Damit jeder Charakter die Möglichkeit hat, in das Spielgeschehen
einzugreifen, geschieht dies in einer festgelegten Reihenfolge.

Dabei leitet ein Charakter nach dem anderen eine Interaktion ein.

Eine Interaktion besteht dabei aus einer Probe und dem damit verbundenen
Würfeln. Manche Interaktionen bestehen aus mehr als einer Probe, so zum 
Beispiel bei der Interaktion mit anderen Charaktern. Mehr dazu später.

Nachdem der Ausgang der Probe und dessen unmittelbare Folgen festgestellt
wurden, kann der nächste Spieler seine Interaktion durchführen.

!!! note "Hinweis"

    Das strikte Einhalten dieser Regel wird nicht empfohlen, inbesondere
    in Situation, in welcher das Charakterspiel vorrang hat. In taktisch
    spannenden Situationen wird das Einhalten einer festgegebenen
    Reihenfolgte empfohlen, damit jeder Charakter eine Möglichkeit hat,
    die Situation zu beeinflußen.

    Für noch mehr taktische Möglichkeiten gibt es die Regelerweiterung [Taktische Zeit](#taktische-zeit).


### Proben und Würfeln

Interaktionen eines Charakters mit der Umwelt werden als Aktionen
bezeichnet. Grundsätzlich kann jede Aktion schief gehen, egal wie begabt
oder erfahren ein Charakter ist. Der Spielleiter kann daher den erfolgreichen
Ausgang jeder Aktion in Frage stellen und verlangen, dass dieser mit einer
erfolgreichen Probe bewiesen wird.

!!! note "Beispiel"

    Tamr würde gerne einen Fluss durchschwimmen. Der Spielleiter bezweifelt,
    dass dies ohne Schwierigkeiten geht und Tamara muss eine Probe auf
    **Schwimmen** ablegen.


Ob und wie erfolgreich die Aktion ausgeführt wird, hängt vom Ausgang der
Probe ab.Eine Probe bezieht sich dabei immer auf eine gemäss der Aktion
relevante Fertigkeit und ein gemäss der Situation relevantes Attribut.

!!! note "Beispiel"

    Der Strom des Flußes ist nicht besonders stark, es hat jedoch ein paar
    prekäre Stellen, die zu meistern es viel Nervenstärke braucht. Der
    Spielleiter legt daher **Emotionale Belastbarkeit** als relevantes
    Attribut für die Probe fest und legt den Zielwert für eine erfolgreiche
    Probe auf fünf fest.

Für die Durchführung der Probe wir eine Anzahl Würfel (W6) geworfen,
welcher der Summe von Fertigkeitswert und Attributswert entspricht. Es
wird offen gewürfelt, so dass das Wurfergebnis für alle einsehbar ist.

!!! note "Hinweis"
    In der Regel wirft der betroffene Spieler für das Geschick seines
    Charakters. Es ist jedoch dem Spielleiter überlassen, dies auch selbst
    in die Hände zu nehmen. In diesem Fall kann der Wurf auch verdeckt
    erfolgen.

!!! note "Wichtig"

    Jede resultierende 4, 5 oder 6 zählt als ein Erfolg.

Die Summe der Erfolge ist das Wurfergebnis. Liegt das Wurfergebnis auf
oder über dem vom Spielleiter festgelegten Zielwert, so gilt die Probe
als bestanden.

Der Spielleiter muss den Zielwert einer Probe nicht bekannt geben.

!!! note "Beispiel"

    Ragolf der Zauberer ist dabei, in eine Falle zu treten. Der Spielleiter
    sieht eine Probe als angemessen, will dies jedoch nicht bekanntgeben, da
    auch ein negativer Ausgang den Spielern einen unangemeßenen Hinweis auf
    die Falle geben würde. Um die Falle zu entdecken ist eine erfolgreiche
    Probe auf **Fallen entdecken** erforderlich. Da es eine wirklich
    bemerkenswerte Falle ist, legt der Spielleiter den Probewert bei 8 an.
    Als relevantes Attribut wird **Mentale Agilität** bestimmt.

    Der Spielleiter würfelt verdeckt, sieht das positive Würfelergebnis war
    und informiert den Spieler darüber, dass Ragolf eine Falle entdeckt hat.


Die Umstände einer Aktion können diese erleichtern oder erschweren. Dem
Spielleiter steht es daher frei, die Anzahl der für die Probe
eingesetzten Würfel zu erhöhen oder zu verringern.

In Anhang A findet sich eine Liste mit Vorschlägen für Modifikationen.

!!! note "Beispiel"

    Tamr würde gerne eine mit Stahlriemen verstärkte Türe aufbrechen. Der
    Spielleiter fordert eine Probe für das Attribut **Physische Stärke** und
    gibt vier Erfolge als den Zielwert an, der für das Aufbrechen der Türe
    erreicht werden muss. Da Tamaras Hände mit Handschellen gefesselt sind,
    erschwert der Spielleiter die Probe um zwei Punkte. Tamara hat beim
    Attribut **Physische Stärke** einen Wert von sechs, abzüglich der
    Erschwernis der Handschellen (zwei Punkte) verbleiben vier Punkte. Sie
    nimmt eine entsprechende Anzahl Würfel und würfelt wie folgt: 1,4,5,1.

    Die Vier und die Fünf zählen als Erfolge; Tamara hat also zwei Erfolge
    erzielt; zu wenig um die Türe aufzubrechen.


#### Aktive und passive Proben




#### Unterstützen

Ein Charakter kann einen anderen bei einer Probe unterstützen. Dazu muss entweder
der 

#### Ausgedehnte Probe

Manchmal erfordern Aktionen mehrere aufeinanderfolgende Proben. In
diesem Fall spricht man von einer ausgedehnten Probe. Diese dient dazu,
die Erfolgsaussichten eines Charakters bei einer langwierigen und
komplexen Aktion abzuschätzen.

Die Anzahl der Proben und der Zielwert werden vom Spielleiter
festgelegt. Jede erfolgreiche Probe trägt zum Fortschritt der Aktion
bei, während ein Misserfolg den Fortschritt behindern oder gar
zunichtemachen kann.

Der Spielleiter kann bei ausgedehnten Proben auch Ereignisse oder
Komplikationen einbauen, die den Fortschritt beeinträchtigen oder den
Charakteren zusätzliche Herausforderungen bieten.

!!! note "Beispiel"

    Die Charaktere wollen einen versteckten Tempel im Dschungel finden und
    müssen dafür eine ausgedehnte Probe auf **Orientierung** ablegen. Der
    Spielleiter legt fest, dass insgesamt 18 Erfolgspunkte notwendig sein,
    um den Tempel zu finden. Jede Probe steht dabei für einen Tag Suche im
    Dschungel. Bei jedem Wurf kann der Spielleiter Ereignisse einbauen, die
    den Fortschritt behindern, wie etwa Krankheiten, Begegnungen mit
    gefährlichen Tieren oder Wetterkapriolen.

### Interaktionen zwischen Charaktern

In der Spielwelt interagieren Charaktere nicht nur mit der Umgebung,
sondern auch miteinander und werden immer von einem Charakter, dem
Initiator, begonnen. Diese Interaktionen können verschiedene Formen
annehmen.

#### Einfache Interaktionen, Interaktionen und Wettkampf

Einfache Interaktionen zwischen Charakteren können in der Regel ohne
Proben oder Würfel auskommen. Zum Beispiel, wenn ein Charakter einem
anderen einen Gegenstand gibt oder eine einfache Konversation führt.

Wenn es jedoch darum geht, jemanden zu überzeugen oder zu beeinflussen,
kann eine Probe auf eine passende Fähigkeit und Attribut notwendig sein.
Die Schwierigkeit und der Zielwert der Probe hängen von der Situation
und den beteiligten Charakteren ab.

Wenn zwei Charaktere in direktem Wettbewerb zueinander stehen, wie zum
Beispiel bei einem Kampf, einem Wettrennen oder einem Wettstreit, wird
dies als Wettkampf bezeichnet. Hier werden die relevanten Fähigkeiten
und Attribute beider Charaktere verglichen und es wird eine Probe
durchgeführt, um den Sieger zu ermitteln.


### Konflikt

Möchte ein Charakter die Aktion eines anderen verhindern, so handelt es
sich dabei um einen Konflikt. Dabei kann es sich zum Beispiel um einen
Kampfe, eine Diskussion oder auch ein Wettdichten handeln.

Bei Proben eines Konfliktes werden die Proberesultate verglichen.

\## Nahkampf - Vergleichender Fertigkeitswurf beider Parteien -
anschliessend Schadenswiderstandwurf auf Attribut+Fertigkeit, Differenz
als Schaden - Schaden reduziert durch Rüstung - Schaden erhöht durch
Waffeneigenschaften

\## Fernkampf - Fertigkeitswurf des Angreifers, Malus durch Entfernung,
Sicht, Bewegung, Witterung - anschliessender Schadenwiderstandswurf auf
Attribut(+Fertigkeit), Differenz als Schaden - Schaden reduziert durch
Rüstung - Schaden erhöht durch Waffeneigenschaften

\## Magie - Bestimmen der Magnitude/Stufe durch den Angreifer -
Fertigkeitswurf des Angreifers, Malus durch Umgebung, Distanz - Falls
Magnitude nicht erreicht wird; Differenz als Schaden für Angreifer -
Falls Magnitude erreicht wird; Magnitude als Schaden - Falls Magnitude
überschritten wird; Magnitude plus Hälfte der Überschreitung(\...) -
Schadenswiderstandswurf auf Attribut+Fertigkeit (????) - Schaden
reduziert durch Artefakte

\## Für den Spielleiter \### Allgemeine Tipps \### Ausrüstung und andere
Modifikatoren \### Monster und Rivalen \### Ein Abenteuer erstellen \###
Erfahrungspunkte verteilen

#### Austausch

Ein Austausch besteht aus einer die Aktion und - möglicherweise - einer
Gegenaktion.

Eine Probe wird von einem Charakter abgelegt, um eine Aktion
durchzuführen, während eine Gegenprobe von einem anderen Charakter
abgelegt wird, um zu versuchen, die Aktion des ersten Charakters zu
verhindern oder zu beeinflussen. Dabei

!!! note "Beispiel"

    Anna möchte den Wachmann davon überzeugen, dass sie und ihre Gruppe zur
    Party im Schloss gehören. Der Spielleiter fordert eine Probe auf die
    Fähigkeit **Überreden + Emotionale Stärke**. Der Wachmann legt eine
    passive Probe auf **Willenstärk + Emotionale Belastbarkeit** ab. Anna
    würfelt eine 4, der Wachmann eine 5. Anna ist es nicht gelungen, den
    Wachmann zu überzeugen.


Die Anzahl der geworfenen Würfel wird wie bei normalen Proben berechnet,
wobei die Summe von Fertigkeits- und Attributswert des Charakters die
Anzahl der Würfel bestimmt. Die Anzahl der Erfolge bestimmt, ob die
Aktion erfolgreich ist oder nicht; der Charakter

## Konflikte

In Konfliktsituationen, wie Kämpfen oder verbalen Streitereien, können
mehrere Proben nacheinander erfolgen, bis ein Sieger feststeht. Jeder
Charakter hat eine bestimmte Anzahl an Lebenspunkten, die durch Schaden
reduziert werden können. Wenn ein Charakter keine Lebenspunkte mehr hat,
ist er besiegt oder tot, je nach Situation.

Während des Kampfes können die Charaktere verschiedene Aktionen
ausführen, wie Angriffe, Verteidigung oder Flucht. Jede Aktion erfordert
eine Probe, um zu bestimmen, ob sie erfolgreich ist oder nicht.

### Kampf

Ein Kampf besteht aus mehreren \[\[Schlagabtausch\]\]en zweier
Kontrahenten, bis einer der beiden entweder aufgibt oder
handlungsunfähig ist.

Frage: Wie sieht ein \'emotionaler\' Kampf aus? Ein \'mentaler\' Kampf?

Schlagabtausch Beide Charakter legen eine Probe ab. Die höhere Anzahl
Erfolge gewinnt den Schlagabtausch. Der unterlegene Charakter erleidet
Schaden in Höhe der Differenz der Anzahl Erfolge der beiden Proben.
Werden beim Schlagabtausch Waffen verwendet, so wird der Waffenschaden
zu Schaden hinzugerechnet.

#### Wettbewerb

Ein Wettkampf bei dem zwei oder mehr Charakter um die Bestleistung
wetteiffern. Jeder Teilnehmer des Wettkampfs führt eine \[\[Ausgedehnte
Probe\]\] durch. Der Spielleiter legt einen Zielwert oder eine
Maximalrundenzahl fest. Sieger des Wettkampfs ist der Charakter, welcher
zuerst den Zielwert erreicht oder, falls eine Maximalrundenzahl
festgelegt wurde, der Charakter mit der höchsten Anzahl kummulierter
Erfolgspunkte.

!!! note "Beispiel"

    Sandra und Mirko rennen um die Wette. Der Spielleiter legt den Zielwert
    6 vor. Beide Charakter rennen.
    In der ersten Runde rennt Sandra 3 Punkte, Mirko 4.
    In der zweiten Runde rennnt Sandra 4 Punkte und Mirko 5.
    In der dritten Runde rennt Sandra 4 Punkte und Mirko einen Punkt.
    
    Mirko erreicht als erster den Zielwert 10, da die Punkte für das
    Erreichen des Zielwertes einzeln addiert und verglichen werden.


#### Schaden

Bei der Interaktion mit der Spielwelt Charaktern kann es sich zutragen,
dass ein Charakter Schaden erfährt. Schaden bezieht sich immer auf ein
spezifisches Attribut und reduziert die für Proben zur Verfügung
stehende Anzahl Würfel; pro Punkt entstandenem Schaden wird die Anzahl
Würfel um eins reduziert.

Ein Punkt Schaden muss in jedem Fall auf das betroffene Attribut
verteilt werden, die verbleibenden Schadenspunkgte können nach belieben
auf das betroffene Attribut und dessen Nachbarattribute (oben und unten)
verteilt werden.

!!! note "Beispiel"

    Hier braucht es ein Beispiel.

Wird ein Attribut durch Schaden auf Null reduziert, so ist der Charakter
nicht mehr handlungsfähig.

### Handlungsunfähig

Übersteigt der gesamte Schaden eines Attributes dessen Wert, so wird der
Charakter handlungsunfähig. Er ist katatonisch starr, kann nichts mehr
im Fokus halten, ist nicht mehr zu denken in der Lage, kann die
Situation nicht mehr erfassen, ist in Selbstmitleid und Zweifel
zusammengebrochen, hat keinen Willen mehr, ist nicht mehr fähig seine
Glieder zu steuern, wurde von seinen Wunden übermannt oder ist vor
Erschöpfung zusammen gebrochen. Je nachdem, um welches Attribut es sich
handelt.

# Appendix A: Listen und Tabellen

## Liste mit Fertigkeiten

Diese Liste enthält lediglich eine begrenzte Anzahl von Beispielen.

Beispiel

:   Definition



## Beispiele für Probenmodifikatoren

### Situation

- Zur Verfügung stehende Ausrüstung
- 


# Appendix B: Regelerweiterungen

## Risky Dice 

Diese Zusatzregel machen die Erfolge gelegentlich ein wenig spektakulärer
und führt zugleich die Möglichkeit des kritischen Scheiterns ein.

-   **Regel der 6**
    Jede bei einer Probe geworfene 6 wird als Erfolg
    gezählt und der Würfel kann erneut geworfen werden. Auch für diesen
    erneuten Wurf -- und jeden weitern --- gilt die Regel der 6.


 **Kritisches Scheitern** Ist mehr als die Hälfte der Würfel eine 1,
    so gilt dies als \_kritisches [Scheitern](); die Aktion ist nicht
    nur gescheitert; der Charakter erleidet einen erheblichen Nachteil,
    wie zum Beispiel zusätzlichen Schaden oder eine andersweitige,
    ungünstige Veränderung der Siel-Situation.


## Kampagnenspiel: Erfahrungspunkte und Charakteraufstieg

Square ist prinzipiell für One-Shots konzipiert; Spiele, welche in einem
Durchgang durchgespielt werden. Manchmal jedoch geniesst ein One-Shot solche
Popularität, dass sich eine Kampagne daraus entwickelt. Dazu werden ein paar
zusätzliche Regelmechanismen notwenig, um die Entwicklung des Charakters durch
das Erlebte und das Gelernte wiederzuspiegeln.

### Erfahrungspunkte

In jedem Spiel sammelt ein Charakter Erfahrung. Dies wird
durch Erfahrungspunkte abgebildet. Pro Spiel werden ein bis
drei Erfahrungspunkte verteilt.

!!! note "Wichtig" 
    Die Anzahl der vergebenen Erfahrungspunkte wird vom Spielleiter bestimmt.
    
### Attributswerte steigern

Attribute steigern Um ein Attribut um eine Stufe zu steigern, muss
der neue Attributswert quadriert ausgegeben werden.

!!! note "Beispiel"
    Maxime will ihre **Phyische Stärke** von 4 auf 5 steigern. Dies kostet sie 
    fünfundzwanzig Erfahrungspunkte.

### Fertigkeiteswerte steigern

Um den Wert einer Fähigkeit um eins zu steigern, muss der neue Fertigkeitswert
ausgegeben werden. Das Steigern eines Fertigkeitswertes muß durch Ereignisse im
Spiel begründet sein.

!!! note "Beispiel"
    Maxime will ihre Fertigkeit **Gewichtheben** von 4 auf 5 steigern.
    Dies kostet sie fünf Erfahrungspunkte.


### Fertigkeiten lernen

Neue Fertigkeiten können in Absprache mit dem
Spielleiter erlernt werden. Neue Fertigkeiten sollten durch Ereignisse
im Spiel begründet sein.


## Komplexer Schaden: Erschöpfung und Wunde

Es gibt zwei Arten von Schaden; Erschöpfung und Wunden. 


### Erschöpfung
Erschöpfung ist non-permanente Leistungssenkung und ist nach
einer Ruhepause wiederhergestellt.

Jeder Punkt Erschöpfung reduziert die für eine Probe zur Verfügung
stehenden Wüfel um eins. Wird für die Probe eine erlernte Fertigkeit
verwendet, so wird Erschöpfung in der Höhe des Fertigkeitswertes
ignoriert.

### Wunden

Jede Punkte Wunde reduziert die für eine Probe zur Verfügung stehenden
Würfel um eins.

Die Heilung von Wunden muss im Spiel durch signifikante Handlungen
vorbereitet, vorangetrieben und betreut werden.
 

## Wurf wiederholen

!!! note Wichtig 
    Basiert auf der Regelerweiterung [Komplexer Schaden: Erschöpfung und Trauma](#komplexer-schaden-erschöpfung-und-trauma)

Ist ein Spieler mit dem Resultat einer Probe nicht einverstanden, so
kann der Wurf wiederholt werden. Jede Wiederholung eines Wurfes
verursacht einen Punkt [Erschöpfung](#er), diese wird für Abzüge wirksam,
sobald der Spieler mit dem Wurf zufrieden ist.

Ein Wurf kann wiederholt werden, so lange ein Charakter Erschöpfung
erleiden kann. Erschöpfung durch Wurf-Wiederholungen muss hierbei
eingerechnet werden, auch wenn die Erschöpfung für die
Wiederholungswürfe keine Abzüge verursacht.

Ist der Spieler mit dem Ergebnis des Wufes zufrieden oder nicht
wiederholt werden, so wird die gesamte Erschöpfung aller Wiederholungen
wirksam.

!!! note "Beispiel"

    Georg möchte gerne von einem Hausdach auf das nächste Springen. Da die
    beiden Hausdächer durch eine beträchtliche Distanz getrennt sind,
    verlangt der Spielleiter eine erfolgreiche Probe von vier Erfolgen auf
    **Physische Stärke + Weitsprung**. Georg hat fünf Punkte beim Attribut
    **Physische Stärke** und zwei Punkte bei der Fertigkeit \"Weitsprung\".
    Zudem holt Georg den notwendigen Anlauf und erhält dadurch einen Bonus
    von 3. Der Spieler würfelt drei Erfolge; nicht genug um die Probe zu
    bestehen. Der Spieler ist mit diesem Ergebnis nicht zufrieden und
    würfelt erneut; dieses Mal sind es die zu erwartenden fünf Erfolge und
    der Spieler ist zufrieden; Georg kann zu dem benachtbarten Hausdach
    springen. Für jede Wiederholung des Wurfes notiert sich der Spieler
    einen Punkt Erschöpfung auf das verwendete Attribut. Da es in unserem
    Beispiel eine Wiederholung gab, wird ein Punkt Erschöpfung auf
    **Physische Stärke** notiert.


Als Proberesultat dient das vom Spieler ausgewählte Wurfergebnis. Dabei
muss es sich nicht um das beste Resultat handeln.


## Taktische Zeit

Die Spieler agieren in festgelegter Reihenfolge.
Bei seinem Zug kann ein Spieler einen passive Probe auf **Physische Stärke** ablegen.

Die Anzahl der Erfolge plus der Wert von **Physischer Stärke** wird in einem Aktionspool verbucht. Pro sechs Punkte
kann ein Charakter eine Aktion durchführen.


!!! note "Beispiel"
    
    Tia, Lars und Anna agieren in Folge.
    Tia hat **Physische Stärke: 4**, sie hat 2 Erfolge, was in sechs Punkten im Aktionspool führt. Sie kann eine Aktion ermöglicht.
    Lars hat **Physische Stärke: 5**, er hat ebenfalls zwei Erfolge. Er kann eine Aktion ausführen und zudem einen Punkt für die nächste Rune aufbewahren.
    Anna hat **Physische Stärke: 3**, sie hat einen Erfolg, wodurch **3 + 1** Punkte im Aktionspool verbucht werden.


## Konflikte


\## Optionale Zusatzregeln


\### Affinität

\### Tactical Square Schaden wird in zwei Kategorien aufgeteilt;
Erschöpfung und Trauma.

\### Magie Für Zauber muss vor dem Wurf ein Zielwert angegeben werden.
Wird der Zielwert nicht erreicht, so erleidet der Charakter die
Differenz zwischen Zielwert und Wurfergebnis als Schaden. Erschöpfung
oder Trauma, je nach Schadensart des Zaubers

Keine Wurfwiederholung bei Zaubern. Stattdessen \'\[Recasting\]\'

\#### Recasting Zauber kann mit gleichem Zielwert wiederholt werden.
Erlittener Schaden tritt nach Abschluss der Aktion in Kraft, kann normal
verteilt werden. Eine zusätzliche Erschöpfung auf das Wurfattribut.

\## Beispiele

\### Charaktererschaffungs mit Aufstieg

\## Liste mit Modifikatoren für Proben

\## Liste mit Waffenschaden

Messer: 1 Dolch: 3 Degen: 4 Beil: 4 Schusswaffe: 8

\## Zusammenfassungen

\## Charakterbogen

Beispiele benötigt,verlinken


\</style\>
